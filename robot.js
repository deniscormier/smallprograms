// Copyright (c) 2013 Denis Cormier <denis.r.cormier@gmail.com>
// See the file license.txt for copying permission

// Javascript constructor function
// Used by the server to instantiate the robot
function Robot(robot) {}

// Called when I am idle
// In other words, I am not in any of the other functions
Robot.prototype.onIdle = function(ev) {
  // Tactic: Spin on self to locate enemy robot and advance
  // Be super aggressive
  var robot;
  robot = ev.robot;
  robot.turnRight(6);
  robot.ahead(1);
};

// Called when I come in contact with another robot
Robot.prototype.onRobotCollision = function(ev) {
    // Don't do anything special for a robot collision
  // We are already doing enough while responding to other events
};

// Called when I hit a wall
Robot.prototype.onWallCollision = function(ev) {
  // Back out and rotate in the direction decided by the life percentage total
  var robot;
  robot = ev.robot;
  var direction = 1;
  if (robot.life < 50) direction = -1;
  robot.back(25);
  robot.turn(90 * direction);
};

// Called when I see another robot
Robot.prototype.onScannedRobot = function(ev) {
  var robot;
  robot = ev.robot;
  if (robot.parentId != ev.scannedRobot.id && robot.id != ev.scannedRobot.parentId) {
    // Parent won't shoot clone, and clone won't shoot parent
    robot.fire();
    robot.ahead(25);  // Advance on target and be aggressive
    robot.turnLeft(30);  // "Undo" a bit of rotation to pass over again a split second later
  }
};

// Called when a bullet hits me
Robot.prototype.onHitByBullet = function(ev) {
  var robot;
  robot = ev.robot;
  if (robot.availableClones > 0) robot.clone();  // Clone when entering battle
  // Vanish when in danger
  if (robot.life < 50 && robot.availableDisappears > 0) robot.disappear();
  robot.turn(ev.bearing);  // Face in the direction that the bullet came from
};
