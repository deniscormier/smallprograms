REM Copy to desktop
REM cmd /k to keep the window open

REM From x to y example
REM cmd /k ffmpeg -i obs-2016-04-21-2027-21.mp4 -ss 4372 -to 6441 -c copy spelunky_hell_26_27_262.mp4
REM cmd /k ffmpeg -i obs-2016-04-21-2027-21.mp4 -ss 01:12:52 -to 01:47:21 -c copy spelunky_hell_26_27_262.mp4

REM From x, for y seconds example
REM cmd /k ffmpeg -i obs-2016-04-21-2027-21.mp4 -ss 4372 -t 2069 -c copy spelunky_hell_26_27_262.mp4
REM cmd /k ffmpeg -i obs-2016-04-21-2027-21.mp4 -ss 1:12:52 -t 34:29 -c copy spelunky_hell_26_27_262_2.mp4

REM Audio offset correction
REM ...BUG! The program doesn't seem to register offsets less than 1 second?
REM 0:v means map first file's video to output file where 1:a is audio from second input file
REM Example below shows video being delayed by 1.25 seconds
REM cmd /k ffmpeg -itsoffset 0.0 -i obs-spelunky-4_02_162.mp4 -itsoffset 1.25 -i obs-spelunky-4_02_162.mp4 -ss 00:57:05 -t 00:06:55 -map 1:v -map 0:a -vcodec copy -acodec copy spelunky_low_4_02_162_youtube.mp4

REM slow down video
REM cmd /k ffmpeg -i clip.mp4 -filter_complex "[0:v]setpts=8.0*PTS[v];[0:a]atempo=0.5,atempo=0.5,atempo=0.5[a]" -map "[v]" -map "[a]" got_over_that_spider_somehow_super_slowmo.mp4

REM concatenate 2 videos
REM ffmpeg -i got_over_that_spider_somehow_3.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts intermediate1.ts
REM ffmpeg -i got_over_that_spider_somehow_slowmo.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts intermediate2.ts
REM ffmpeg -i "concat:intermediate1.ts|intermediate2.ts" -c copy -bsf:a aac_adtstoasc got_over_that_spider_somehow_final.mp4

cmd /k ffmpeg -i big_money_13_12_151_footage.mp4 -ss 1:07:05 -c copy big_money_13_12_151.mp4