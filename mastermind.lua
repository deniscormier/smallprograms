--[[
-- mastermind-cmd
--]]

local valid_symbols_array = {"Q", "W", "E", "R", "T", "Y"}

function main()
	math.randomseed(os.time())

	valid_symbols_hash = {}
	for key, value in pairs(valid_symbols_array) do
		valid_symbols_hash[value] = true;
	end

	print("==================================================")
	print("Welcome to Mastermind!")
	print("Guess the target word using the following symbols: ");
	for key, value in pairs(valid_symbols_array) do
		io.write(value .. " ")
	end
	print("\nLookout for clues:")
	print("C: Correct symbols in the correct position")
	print("c: Correct symbols in the incorrect position")
	print("i: Incorrect symbols")
	print("==================================================\n")


	target_word = {
		valid_symbols_array[math.random(#valid_symbols_array)],
		valid_symbols_array[math.random(#valid_symbols_array)],
		valid_symbols_array[math.random(#valid_symbols_array)],
		valid_symbols_array[math.random(#valid_symbols_array)]
	}

	print("A 4-digit number has been generated. Can you guess it?")
	--print("Target word: " .. target_word[1] .. target_word[2] .. target_word[3] .. target_word[4] .. "\n")

	guess_attempt = 1
	while true do
		print("\nAttempt " .. guess_attempt)
		io.write("Enter 4 symbols: ")

		while true do
			guess_string = io.read()

			guess_word = {
				string.sub(guess_string, 1, 1),
				string.sub(guess_string, 2, 2),
				string.sub(guess_string, 3, 3),
				string.sub(guess_string, 4, 4),
			}
			if guess_is_valid(guess_word) then
				break
			end

			print("An invalid guess was provided. Try again.")
			io.write("Enter 4 symbols: ")

		end
		correct_sym_correct_pos, correct_sym_incorrect_pos, incorrect_sym = compare_words(guess_word, target_word)
		print_status(correct_sym_correct_pos, correct_sym_incorrect_pos, incorrect_sym)

		if correct_sym_correct_pos == 4 then
			print("You have guessed the target word correctly after " .. guess_attempt .. " attempts. Congratulations!")
			break
		end

		guess_attempt = guess_attempt + 1
	end
end

function guess_is_valid(guess_word)
	for key, value in pairs(guess_word) do
		if valid_symbols_hash[value] == nil then
			return false
		end
	end
	return true
end

function compare_words(guess_word, target_word)
	correct_sym_correct_pos = 0
	correct_sym_incorrect_pos = 0
	incorrect_sym = 0

	--[[
	for key, value in pairs(guess_word) do
		print(value)
	end
	print("")
	for key, value in pairs(target_word) do
		print(value)
	end
	print("")
	]]

	--[[
	For each valid symbol, look at where they appear in both arrays.
	Easy to tell if the correct symbol is in the correct position
	Match up occurences
	--]]

	matched_positions = {}
	for key, value in pairs(valid_symbols_array) do
		guess_word_positions_for_letter = {}
		target_word_positions_for_letter = {}
		for key2, value2 in pairs(guess_word) do
			if value == value2 then
				guess_word_positions_for_letter[key2] = true
			end
		end
		for key2, value2 in pairs(target_word) do
			if value == value2 then
				target_word_positions_for_letter[key2] = true
			end
		end

		-- Find correct symbols in correct position
		for i = 1, 4 do
			if guess_word_positions_for_letter[i] == true and target_word_positions_for_letter[i] == true then
				matched_positions[i] = true
				correct_sym_correct_pos = correct_sym_correct_pos + 1
				guess_word_positions_for_letter[i] = nil
				target_word_positions_for_letter[i] = nil
			end
		end

		-- Find correct symbols in incorrect position
		for i = 1, 4 do
			if guess_word_positions_for_letter[i] == true then
				for j = 1, 4 do
					if target_word_positions_for_letter[j] == true then
						correct_sym_incorrect_pos = correct_sym_incorrect_pos + 1
						guess_word_positions_for_letter[i] = nil
						target_word_positions_for_letter[j] = nil
						break
					end
				end
			end
		end
	end

	incorrect_sym = 4 - correct_sym_correct_pos - correct_sym_incorrect_pos
	return correct_sym_correct_pos, correct_sym_incorrect_pos, incorrect_sym
end

function print_status(correct_sym_correct_pos, correct_sym_incorrect_pos, incorrect_sym)
	--print(correct_sym_correct_pos .. correct_sym_incorrect_pos .. incorrect_sym)
	while correct_sym_correct_pos > 0 do
		io.write("C")
		correct_sym_correct_pos = correct_sym_correct_pos - 1
	end
	while correct_sym_incorrect_pos > 0 do
		io.write("c")
		correct_sym_incorrect_pos = correct_sym_incorrect_pos - 1
	end
	while incorrect_sym > 0 do
		io.write("i")
		incorrect_sym = incorrect_sym - 1
	end
	print("")
end

main()
