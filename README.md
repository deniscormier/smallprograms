License
=======

The usage of my code falls under the terms of the MIT/X11 license (see the LICENSE file or see http://opensource.org/licenses/MIT) except otherwise noted.

robot.js
========

http://fightcodegame.com hosts a game that pits robots against each other. robot.js holds the Javascript code that represent the robot's actions. The Javascript code interacts with various functions and parameters from a carefully designed interface (http://fightcodegame.com/docs).
